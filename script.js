let trDown = (itr, symbol) => {
  for (let j = itr + 1; j < 10; j++) {
    document.write(symbol);
  }
};

let trUp = (itr, symbol) => {
  for (let y = itr + 1; y > 0; y--) {
    document.write(symbol);
  }
};

// трикутник

let trianglRect = () => {
  for (let i = 0; i < 10; i++) {
    trUp(i, "*");
    document.write("<br>");
  }
};

let triangleUp = () => {
  for (let i = 0; i < 10; i++) {
    trDown(i, "&nbsp");
    trUp(i, "*");
    document.write("<br>");
  }
};

let triangleDown = () => {
  for (let i = 0; i < 10; i++) {
    trUp(i, "&nbsp");
    trDown(i, "*");
    document.write("<br>");
  }
};

// ромб

let rhombus = () => {
  triangleUp();
  triangleDown();
};

// прямокутник

let rectangle = () => {
  for (let i = 0; i < 10; i++) {
    if (i === 0 || i === 9) {
      for (let j = 0; j < 20; j++) {
        document.write("*");
      }
    } else {
      for (let x = 0; x < 20; x++) {
        if (x === 0 || x === 19) {
          document.write("*");
        } else {
          document.write("&nbsp;&nbsp;");
        }
      }
    }
    document.write("<br>");
  }
  document.write("<br>");
};

// виклик функцій (фігур)

trianglRect();
triangleUp();
rhombus();
rectangle();
